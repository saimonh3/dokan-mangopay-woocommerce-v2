=== MANGOPAY WooCommerce ===

Contributors: ydubois, mangopay
Tags: woocommerce, wc-vendors, marketplace, payment, gateway, shop, store, checkout, multivendor
Requires at least: 4.4.0
Tested up to: 5.0
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Official WooCommerce checkout gateway for the MANGOPAY payment solution dedicated to marketplaces.


== Description ==

MANGOPAY is a payment solution dedicated to web marketplaces available worldwide.

This is the official WooCommerce payment gateway for MANGOPAY and accepts worldwide credit/debit card payments by CB, Visa and Mastercards as well as Maestro, Diners, iDeal, Bancontact/Mister Cash, PayLib and Masterpass. It supports the following currencies: EUR, GBP, USD, CHF, NOK, PLN, SEK, DKK, CAD, ZAR.

It allows for the management of vendor commissions from within the WordPress administration, including direct payouts of the commissions earnt to the vendor bank accounts.

Here are some of MANGOPAY features supported by this plugin:

* Manage multi-currency (although note that WooCommerce only allows for one base currency at a time)
* Handle international and local payment methods
* Seamlessly escrow and split funds betwen marketplace operator and vendors according to your business model
* Anti-fraud and anti-money laundering processes built in
* Manage the payment process end to end (payins, split funds and then payouts)
* PCI Compliant

For more information about MANGOPAY please consult [the MANGOPAY site](https://www.mangopay.com/ "MANGOPAY")

For support please turn to the [plugin's official support forum](https://wordpress.org/support/plugin/mangopay-woocommerce)

This plugin relies on [WooCommerce](https://www.wordpress.org/plugins/woocommerce/) to turn the WordPress platform into a full-featured e-commerce solution.

This plugin also relies on the [WC-Vendors](https://www.wordpress.org/plugins/wc-vendors/) plugin to turn a WooCommerce-enabled online shop into a multi-vendor marketplace.


= Plugin usage =

The MANGOPAY plugin simply connects your WordPress with the MANGOPAY API - it does not add any visible extra functionality - all the products and orders are handled by the Woocommerce plugin (and payment is done via MANGOPAY) and all vendors/commission is handled by the WC Vendors plugin. Therefore there is no customisation for the MANGOPAY plugin (besides providing your account details).

Buyers can purchase products from several different vendors in the same checkout and the MANGOPAY plugin handles the commission and payouts to each vendor separately

The buying process is as follows:

* An order is placed
* Each vendor marks each product as dispatched
* Once all the vendors have dispatched the items, you mark the order as completed (this transfers the funds from the buyer’s wallet to the vendors’ wallets)
* When you wish, you can dispatch funds from the vendors’ wallets to their bank account (you can either add a bank account for them manually from their user profile page, or they can from their vendor settings page)

You can view more information about your user transactions in the [MANGOPAY Dashboard](https://docs.mangopay.com/mangopay-dashboard/). However, you should not do any manual operations (transfers/payouts etc) as these will not be in sync with the WordPress plugin and hence the workflows impacted will no longer be able to function correctly (eg you do a payout from a vendor’s wallet, but WordPress will not be notified and so it will think there is still commission to be paid out for this vendor)


== Installation ==

= Requirements =

* You will need to have the [WooCommerce](https://www.wordpress.org/plugins/woocommerce/) and [WC-Vendors](https://www.wordpress.org/plugins/wc-vendors/) plugins installed and activated
* You will of course need a WordPress site
* You will not need a developer, nor particular technical experience

= Installation =

* Before starting, make sure your WordPress environment meets the Requirements listed below
* You can install the plugin directly from the usual WordPress plugin directory (automatic plugin installation is fully supported)
* Active the plugin
* Go to the settings page (choose "MANGOPAY" from the main left menu) and enter your ClientId/Passphrase (you can start with just the sandbox if you prefer and add your production credentials later). Save this info
* In the block "MANGOPAY status" there are various checks that are done - you’ll them all in green for the plugin to work correctly with your WordPress
* If you already have users with the role "vendor" (perhaps because you were already using the WC Vendors plugin), you’ll need to add the required fields for these users (_Birthday_, _Nationality_ and _Country of Residence_) otherwise MANGOPAY will not be enabled at the checkout if the basket contains products from these vendors. You can add this info from the WordPress user profile page.
* Note that you need to repeat this step for both sandbox and production environments.
* You may then wish to set the commission for products sold via your side - this is done via the WC Vendors plugin, and can be set globally, for a vendor, or per product
* When you are ready to go live, from the MANGOPAY settings page, choose the "Production" environment and add your credentials (if you haven’t already done so). After saving these changes, verify that all the checks are still in green (and resolve any that are not)


A free sandbox (testing) account is available with MANGOPAY: https://www.mangopay.com/signup/create-sandbox/
It allows marketplace operators to fully test the MANGOPAY solution and try out all payment options before production deployment by simulating the whole process with [test payment cards](https://docs.mangopay.com/api-references/test-payment/).

Use of the MANGOPAY solution in production necessitates registration and prior approval (and takes about 72 hours).
You can sign up for a production account here: https://www.mangopay.com/signup/submit-your-app/go-live/

= Updating =

Automatic updates should work as normal; as always though, ensure you backup your site just in case.


== Frequently Asked Questions ==

= How much does it cost to use the MANGOPAY as a payment solution? =

Please consult the MANGOPAY pricing page: https://www.mangopay.com/pricing/

= How can I display PayPal from the vendor shop settings page? =

See [here](https://www.wcvendors.com/kb/turn-paypal/) on the WC Vendors site

= Can I use a more complicated commission structure? =

Probably, yes! Have a look [here](https://www.wcvendors.com/kb/commissions-fixed-dollar-amount-fixed-plus-percentage/) on the WC Vendors site

= Will the plugin work with my theme? =

Yes! The MANGOPAY plugin is entirely independent of the theme

= Do I have to use WC Vendors and Woocommerce? =

For the time being, yes - but we will possibly make the plugin compatible with other payment gateways and vendor plugins in a future version

= Where can I get support or talk to other users? =

If you get stuck, you can ask for help in the [MANGOPAY Plugin Forum](https://wordpress.org/support/plugin/mangopay-woocommerce). Please supply as much information and detail as you can about your problem

= Where can I contribute to the project? =

Head over to the [MANGOPAY plugin GitHub repository](https://github.com/Mangopay/wordpress-plugin/) - for the moment the repo is private, so send us an email first to get access.

= Can I customise the design of the payment page? =

Sure! Create a normal page with the shortcode [mangopay_payform] and then go to the WooCommerce settings page, and in the "Checkout" tab, choose "MANGOPAY" and then choose the page you just created for the "Use this page for payment template" setting. Note that there are several requirements for the page to be taken into account - see [here](https://docs.mangopay.com/guide/customising-the-design) for more info.

= Does the vendor need a PayPal email address? =

No! When the vendor updates his shop settings (including providing a bank account), if you are getting an error because a PayPal email address is required, you need to update your shortcode to be `[wcv_shop_settings paypal_address="false"]` instead of just `[wcv_shop_settings]` to remove this requirement.

= Using WPEngine.com for your hosting? =

Due to some strict caching on their side, you'll need to manually request something to them in order for the plugin to function correctly. If you login to your [WPEngine account](https://my.wpengine.com/support#general-issue) and open up a live chat session (top right of the page), you should provide the install name or domain name and say you're using the WooCommerce Mangopay payment plugin and need "a cache exclusion for the plugin file path _/wp-content/uploads/mp_tmp/_ and payment gateway call _^/wp-json_ followed by a config refresh". These actions can only be done manually by their lovely support team, and are critical for the plugin to perform correctly.


== Screenshots ==

1. Accept CB, Visa or Mastercards as a WooCommerce checkout payment option
2. Easily manage client and vendor payment transactions from within the WordPress, WooCommerce and WC-Vendors admin screens
3. Simple settings screen and comprehensive status an health-check dashboard all from within de WP admin


== Changelog ==

= 2.5.0 =

Stable version 2.5

* Added support for the Sofort direct web payin method
* Added support for the Giropay direct web payin method
* Ensured official compatibility with WordPress up to version 5.0-alpha
* Ensured official compatibility with WooCommerce up to version 3.3.3
* Ensured official compatibility with WC-Vendors up to version 1.9.14
* Fixed notice for Undefined index: default_business_type due to misnamed default config value
* Fixed "Error: wrong payment amount." Shown Incorrectly (props Michael Compton)
* Fixed integer conversion problems leading to wrongly rounded values (props Michael Compton)

= 2.4.2 =

Bugfix/compatibility release version 2.4.2

* Ensured official compatibility with WordPress up to version 4.9.2
* Ensured official compatibility with WooCommerce up to version 3.2.6
* Ensured official compatibility with WC-Vendors up to version 1.9.13
* Added `mp_allowed_currencies` filter hook applied inside the load_config() function in admin.inc.php
* Added `mp_account_types` filter hook applied inside the load_config() function in admin.inc.php
* Added `mp_commission_due` filter hook to the $total_due inside the vendor_payouts() function in admin.inc.php
* Added `total_shipping` field to values retrieved from the database when calculating vendor_payouts (passed in the `mp_commission_due` filter)
* Added a new status check in the MP status dashboard to issue a warning when products are attributed to admins instead of vendors
* Fixed admin dashboard being blocked when API calls to retrieve failed payouts and fayed KYCs crashed
* Fixed WooCommerce 3.x deprecation warnings when directly accessing order properties
* Fixed PHP 7.1 deprecation warnings: openssl_encrypt() will now be used instead of mcrypt when available
* Improved error and logging of failed payment attempts when the proper user creation workflow was not respected (ie when users are missing mandatory info such as birthdate or nationality)

= 2.4.1 =

Bugfix release version 2.4.1

* Fixed some warning messages related to WooCommerce version 3.x
* Fixed some issues with images in the front-end dashboard with WC-Vendors Pro
* Fixed some front-end styling/display issues with WC-Vendors Pro
* Fixed js conflict issues with WC-Vendors Pro Pro and select2
* Added versioning of CSS and JS scripts
* Prevented rare fatal errors when API is not responding

= 2.4.0 =

Bugfix release version 2.4.0

* Fixed region field not showing correctly on the bank account form
* Fixed support for variable products created via the back-office WC admin
* Reverted partial support of different currencies in the same marketplace

= 2.3.1 =

Bugfix release version 2.3.1

* Fixed KYC doc upload form compatibility issues with WC-Vendors Pro version

= 2.3.0 =

Stable version 2.3:

* Added KYC doc upload form
* Made postal code optional for various countries
* Fixed "User status is required" error in some contexts
* Fixed currency issues with multilingual WooCommerce
* Fixed multiple problems with the "State/County" field
* Fixed some date translation issues with exotic multilingual support
* Added an order note to indicate when in sandbox or production mode
* Checked compatibility with WP 4.8, WC 3.0.7 and WV 1.9
* Added compatibility with the BuddyPress subscription form

= 2.2.1 =

Bugfix release version 2.2.1

* The "Business type" field can now be changed for existing users
* The "User status" is now shown when creating a new user in the wp-admin
* The "Business type" field is removed in the wp-admin for individual users
* The "Business type" field is now correctly enforced in all cases when creating a new user, which will allow creation of the MP user
* Vendor defaults are applied to pending vendors
* User creation defaults have been clarified in all cases

= 2.2.0 =

Stable version 2.2:

* Changed page shown when payments fail or are cancelled (now goes back to checkout)
* Implemented WooCommerce native select2 fields for Country + County/State selectors
* Enforced mandatory County/State data for specific coutries (US, MX, CA)
* Fixed a bug with business type field not showing in some cases
* Fixed a bug where custom payment pages were always used
* Fixed a bug with dates containing accents
* Fixed a bug with webhook verifications
* Improved field validation error messages for bank account data
* Internal code refactoring
* Checked compatibility with WordPress 4.7.1, 4.7.2, 4.7.3 and 4.8

= 2.1.0 =

Stable version 2.1:

* Support of Bankwire Direct payins
* Support of incoming webhooks for Bankwire Direct
* Automatic webhook management (setup / update)
* Improved transaction ID handling
* Improved internal storage of transaction references
* Improved temporary file storage
* Fixed minor PHP notices
* Internal code refactoring
* Checked compatibility with WC-Vendors 1.9.4
* Checked compatibility with WooCommerce 2.6.4
* Checked compatibility with WordPress 4.7

= 2.0.1 =

Bugfix release version 2.0.1

* Fixes a fatal error with set_commission_paid() when completing orders in the WooCommerce admin panel

= 2.0.0 =

Stable version 2:

* Support of optional payment template URL
* Support of the soletrader status
* Fixed bug with unspecified legacy user nationality in the checkout form (will now correctly trigger an error)
* Optimized front-end performance (admin code is no longer loaded when on the front)
* Major code refactoring
* Checked compatibility with WC-Vendors 1.9.3

= 1.0.3 =

Bugfix/compatibility release 3:

* Fixed compatibility with WC Vendors Pro plugin (save bank account info in the front-end store dashboard)
* Fixed potential compatibility issue with third-party plugins (when overriding checkout fields)

= 1.0.2 =

Bugfix release 2:

* Fixed PHP7 issue with bank account data display

= 1.0.1 =

Bugfix release 1:

* Fixed textual date format issues for localized installations that caused birthday date validation errors
* Fixed vendor role detection on some specific setups that prevented the bank account fields to show up
* Cleared PHP warning message for an unset variable when creating a bank account

= 1.0.0 =

Stable version 1:

* Fixed birthday format validation
* Complete localization of birth date format (now uses WordPress date format option)
* Improved birthday date picker (default year + year drop-down)
* Improved admin notices (more health checks)
* Added support for WC Vendor's "Instapay" feature (instant payment/auto-payouts)
* Added a "failed payout" admin dashboard, with transaction ignore/retry features
* Added a "refused KYC document" admin dashboard
* Tested successfully up to WP 4.5.3, WC 2.6.1 and WC Vendors 1.9.1

= 0.4.0 =

Public beta v4:

* Fixed bug that prevented MP user creation when WP users were created while the plugin was inactive
* Fixed bug that caused card type value to be lost upon ajax update of the checkout page
* Fixed bug that prevented wallet transfers to vendors to be performed with virtual/downloadable/bookable products
* Improved synchronization of bank account data
* Improved bank account data validation
* Added a health-check to ensure at least one payment method is enabled
* Improved credit card selection admin

= 0.3.0 =

Public beta v3:

* Improved natural/legal user management
* Vendors can now correctly manage their bank account data from their WC-Vendor shop settings page
* Avoid crashes when an API connection problem occurs upon MANGOPAY user account creation

= 0.2.2 =

First bugfixes for beta version:

* Avoid error 500 when WooCommerce plugin not activated
* Gateway correctly enabled upon initial plugin activation
* Updated admin notices when setup is incomplete

= 0.2.1 =

Full-featured public beta version.

= 0.1.1 =

Full-featured early beta version.
