<?php
/**
 * Ajax methods for MANGOPAY WooCommerce Plugin admin
 * 
 */
class mangopayWCAjax {
	
	/** This will store our mpAccess class instance **/
	private $mp;

	/** Ignored items **/
	private $ignored_failed_po		= array();
	private $ignored_refused_kyc	= array();
	
	/**
	 * Class constructor
	 *
	 */
	public function __construct() {
		
		/** Get the stored hidden/ignored items **/
		$this->ignored_failed_po	= get_option( 'mp_ignored_failed_po', array() );
		$this->ignored_refused_kyc	= get_option( 'mp_ignored_refused_kyc', array() );
		
		/** Admin ajax for failed payouts and KYCs dashboard widget **/
		add_action( 'wp_ajax_ignore_mp_failed_po', array( $this, 'ajax_ignore_mp_failed_po' ) );
		//add_action( 'wp_ajax_retry_mp_failed_po', array( $this, 'ajax_retry_mp_failed_po' ) );
		add_action( 'wp_ajax_ignore_mp_refused_kyc', array( $this, 'ajax_ignore_mp_refused_kyc' ) );
		add_action( 'wp_ajax_failed_transaction_widget', array( $this, 'ajax_failed_transaction_widget' ) );
	}

	/**
	 * Stores a failed payout resource ID as ignored
	 * 
	 */
	public function ajax_ignore_mp_failed_po() {
		if ( !current_user_can( 'manage_options' ) )
			return;
		
		$this->ajax_head();
		$response = null;
		$ressource_id = null;
		
		if( !empty( $_POST['id'] ) )
			$ressource_id = $_POST['id'];
		
		if( $ressource_id && !in_array( $ressource_id, $this->ignored_failed_po ) ) {
			$this->ignored_failed_po[] = $ressource_id;
			$response = update_option( 'mp_ignored_failed_po', $this->ignored_failed_po );
		}
		
		echo json_encode( $response );
		exit;
	}
	
	/**
	 * Stores a refused KYC doc resource ID as ignored
	 *
	 */
	public function ajax_ignore_mp_refused_kyc() {
		if ( !current_user_can( 'manage_options' ) )
			return;
		
		$this->ajax_head();
		$response = null;
		$ressource_id = null;
		
		if( !empty( $_POST['id'] ) )
			$ressource_id = $_POST['id'];
		
		if( $ressource_id && !in_array( $ressource_id, $this->ignored_refused_kyc ) ) {
			$this->ignored_refused_kyc[] = $ressource_id;
			$response = update_option( 'mp_ignored_refused_kyc', $this->ignored_refused_kyc );
		}
		
		echo json_encode( $response );
		exit;
	}

	/**
	 * Our admin dashboard widget
	 * for displaying failed payout transactions and refused KYC docs
	 *
	 */
	public function ajax_failed_transaction_widget() {
		$this->mp = mpAccess::getInstance();
		
		if ( !current_user_can( 'manage_options' ) ) {
			return;
		}
		$mp_failed = $this->mp->get_failed_payouts();

		$ignored_failed_po = get_option( 'mp_ignored_failed_po', array() );
		if( !empty( $mp_failed['failed_payouts']) ) {
			foreach( $mp_failed['failed_payouts'] as $key => $failed_payout ) {
				if( in_array( $failed_payout->ResourceId, $ignored_failed_po ) ) {
					unset( $mp_failed['failed_payouts'][$key] );
				}
			}
		}

		echo '<h3>' . __( 'Failed payouts', 'mangopay' ) . '</h3>';

		if( empty( $mp_failed['failed_payouts']) ) {
			echo '<p><em>' . __( 'No failed payout', 'mangopay' ) . '</em></p>';
		} else {
			echo '<ul>';
			foreach( $mp_failed['failed_payouts'] as $failed_payout ) {

				if( !$payout_a = get_transient( 'mp_failed_po_' . $failed_payout->ResourceId ) ) {
					$payout = $this->mp->get_payout( $failed_payout->ResourceId );
					if( $payout && is_object( $payout) ) {
						$due_ids = array();
						if( preg_match( '/WC Order #(\d+)/', $payout->Tag, $matches ) ) {
							$order_id = $matches[1];	
							global $wpdb;
							$table_name = $wpdb->prefix . mangopayWCConfig::WV_TABLE_NAME;
							$query = "
								SELECT id
								FROM `{$table_name}`
								WHERE order_id = %d;
							";	//AND status='due'; <- in fact the payout may have been refused afterwards
							$query		= $wpdb->prepare( $query, $order_id );
							$due_ids	= $wpdb->get_col( $query );
						}
						$payout_a = array(
							'payout'	=> $payout,
							'due_ids'		=> $due_ids
						);
						set_transient(
							'mp_failed_po_' . $failed_payout->ResourceId,
							$payout_a,
							60*60*24
						);
					}
				}
				$payout = $payout_a['payout'];

				echo '<li class="mp_failed_po_' . $failed_payout->ResourceId . '">';
				echo date_i18n( get_option( 'date_format' ), $failed_payout->Date ) . '<br/>';
				echo $failed_payout->EventType . ' ';

				$tag = preg_replace(
					'/WC Order #(\d+)/',
					"<a href=\"post.php?post=$1&action=edit\">$0</a>",
					$payout->Tag
				);

				echo $tag . ' ';

				/*
				/wp-admin/admin.php?page=pv_admin_commissions
				&_wpnonce=ebe5c12143
				&_wp_http_referer=%2Fwp-admin%2Fadmin.php%3Fpage%3Dpv_admin_commissions
				&action=-1
				&m=0
				&com_status
				&paged=1
				&id%5B0%5D=35&id%5B1%5D=34
				&action2=mp_payout
				*/

				$retry_payout_url = 'admin.php?page=pv_admin_commissions';
				$retry_payout_url = wp_nonce_url( $retry_payout_url );
                $retry_payout_url .= '&action=mp_payout';

				foreach( $payout_a['due_ids'] as $id ){
					$retry_payout_url .= '&id[]=' . $id;
				}
				$retry_payout_url .= '&mp_initial_transaction=' . $failed_payout->ResourceId;

				echo '<a class="ignore_mp_failed_po" data-id="' . $failed_payout->ResourceId . '" href="#">[' . __( 'Ignore', 'mangopay' ) . ']</a> ';
				echo '<a class="retry_mp_failed_po" href="' . $retry_payout_url . '">[' . __( 'Retry', 'mangopay' ) . ']</a> ';
				//echo '<a class="retry_mp_failed_po" data-id="' . $failed_payout->ResourceId . '" href="#">[' . __( 'Retry', 'mangopay' ) . ']</a> ';
				//var_dump( $failed_payout );	//Debug
				//var_dump( $payout );			//Debug

				echo '</li>';
			}
			echo '</ul>';
		} //end if empty( $mp_failed['failed_payouts'])

		$ignored_refused_kyc = get_option( 'mp_ignored_refused_kyc', array() );
		if( !empty( $mp_failed['refused_kycs']) ) {
			foreach( $mp_failed['refused_kycs'] as $key => $refused_kyc ) {
				if( in_array( $refused_kyc->ResourceId, $ignored_refused_kyc ) ) {
					unset( $mp_failed['refused_kycs'][$key] );
				}
			}
		}

		echo '<hr><h3>' . __( 'Refused KYC documents', 'mangopay' ) . '</h3>';
		if( empty( $mp_failed['refused_kycs']) ) {
			echo '<p><em>' . __( 'No refused KYC document', 'mangopay' ) . '</em></p>';
		} else {
			echo '<ul>';
			foreach( $mp_failed['refused_kycs'] as $refused_kyc ) {
				if( !$kyc_doc_a = get_transient( 'mp_refused_kyc_' . $refused_kyc->ResourceId ) ) {
					$kyc_doc = $this->mp->get_kyc( $refused_kyc->ResourceId );
						
					/** We store a different mp_user_id for production and sandbox environments **/
					$umeta_key = 'mp_user_id';
					if( !$this->mp->is_production() ) {
						$umeta_key .= '_sandbox';
					}
					$wp_user_id = 0;
					$wp_users = get_users( array(
						'meta_key'		=> $umeta_key,
						'meta_value'	=> $kyc_doc->UserId
					));
					if( $wp_users && is_array( $wp_users) ) {
						$wp_user = $wp_users[0];
					}
					if( $kyc_doc && is_object( $kyc_doc) ) {
						$kyc_doc_a = array(
							'kyc_doc'	=> $kyc_doc,
							'wp_user'	=> $wp_user
						);
						set_transient(
							'mp_refused_kyc_' . $refused_kyc->ResourceId,
							$kyc_doc_a,
							60*60*24
						);
					}
				}
				$kyc_doc = $kyc_doc_a['kyc_doc'];

				echo '<li class="mp_refused_kyc_' . $refused_kyc->ResourceId . '">';

				echo date_i18n( get_option( 'date_format' ), $refused_kyc->Date ) . '<br/>';
				echo $refused_kyc->EventType . ' ';
				echo $kyc_doc->Type . ' ';
				echo $kyc_doc->Status . ', ';
				echo $kyc_doc->RefusedReasonType . ' ';

				if( $wp_user_id = $kyc_doc_a['wp_user'] ) {
					echo __( 'For WP user:', 'mangopay' ) . ' ';
					echo '<a href="user-edit.php?user_id=' . $kyc_doc_a['wp_user']->ID . '">';
					echo $kyc_doc_a['wp_user']->user_login . ' ';
					echo '(' . $kyc_doc_a['wp_user']->display_name . ')';
					echo '</a> ';
				} else {
					echo __( 'For MP user:', 'mangopay' ) . ' ';
					echo $kyc_doc->UserId . ' ';
				}

				$upload_url = $this->mp->getDBUploadKYCUrl( $kyc_doc->UserId );
				echo '<a class="ignore_mp_refused_kyc" data-id="' . $refused_kyc->ResourceId . '" href="#">[' . __( 'Ignore', 'mangopay' ) . ']</a> ';
				echo '<a class="retry_mp_refused_kyc" target="_mp_db" href="' . $upload_url . '">[' . __( 'Upload another document', 'mangopay' ) . ']</a> ';

				//var_dump( $refused_kyc );		//Debug
				//var_dump( $kyc_doc_a );		//Debug

				echo '</li>';
			}
			echo '</ul>';
		}
		?>
		<script>
		(function($) {
			$(document).ready(function() {
				//console.log('document ready...');	//Debug
				$('.ignore_mp_failed_po').on( 'click', function( e ){
					e.preventDefault();
					//console.log('clicked ignore_mp_failed_po!');		//Debug
					//console.log(e);				//Debug
					//console.log(this);			//Debug
					//console.log(this.dataset.id);	//Debug
					resource_id = this.dataset.id;
					$.post( ajaxurl, {
						action: 'ignore_mp_failed_po',
						id: resource_id
					}, function( data ) {
						//console.log( data );		//Debug
						if( true === data ) {
							class_id = 'li.mp_failed_po_' + resource_id;
							//console.log( 'hiding: ' + class_id );	//Debug
							$(class_id).hide('slow');
						}
					}).done(function() {
						//console.log( "Ajax ignore_mp_failed_po success" );	//Debug
					}).fail(function() {
						console.log( "Ajax ignore_mp_failed_po error" );	//Debug
					}).always(function() {
						//console.log( "Ajax ignore_mp_failed_po finished" );	//Debug
					});
				});
				/* UNUSED
				$('.retry_mp_failed_po').on( 'click', function( e ){
					e.preventDefault();
					//console.log('clicked retry_mp_failed_po!');		//Debug
					//console.log(e);				//Debug
					//console.log(this);			//Debug
					//console.log(this.dataset.id);	//Debug
					resource_id = this.dataset.id;
					$.post( ajaxurl, {
						action: 'retry_mp_failed_po',
						id: resource_id
					}, function( data ) {
						console.log( data );		//Debug
						if( true === data ) {
							class_id = 'li.mp_failed_po_' + resource_id;
							//console.log( 'hiding: ' + class_id );	//Debug
							$(class_id).hide('slow');
						}
					}).done(function() {
						//console.log( "Ajax retry_mp_failed_po success" );	//Debug
					}).fail(function() {
						console.log( "Ajax retry_mp_failed_po error" );	//Debug
					}).always(function() {
						//console.log( "Ajax retry_mp_failed_po finished" );	//Debug
					});
				});
				*/
				$('.ignore_mp_refused_kyc').on( 'click', function( e ){
					e.preventDefault();
					//console.log('clicked ignore_mp_refused_kyc!');		//Debug
					//console.log(e);				//Debug
					//console.log(this);			//Debug
					//console.log(this.dataset.id);	//Debug
					resource_id = this.dataset.id;
					$.post( ajaxurl, {
						action: 'ignore_mp_refused_kyc',
						id: resource_id
					}, function( data ) {
						//console.log( data );		//Debug
						if( true === data ) {
							class_id = 'li.mp_refused_kyc_' + resource_id;
							//console.log( 'hiding: ' + class_id );	//Debug
							$(class_id).hide('slow');
						}
					}).done(function() {
						//console.log( "Ajax ignore_mp_refused_kyc success" );	//Debug
					}).fail(function() {
						console.log( "Ajax ignore_mp_refused_kyc error" );	//Debug
					}).always(function() {
						//console.log( "Ajax ignore_mp_refused_kyc finished" );	//Debug
					});
				});
			});
		})( jQuery );
		</script>
		<?php
		wp_die();   
	}
	
	private function ajax_head() {
		session_write_close();
		header( "Content-Type: application/json" );
	}
}
new mangopayWCAjax();
?>