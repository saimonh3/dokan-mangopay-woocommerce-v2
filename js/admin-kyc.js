(function($) {

	var url = window.location.href;
	
	$(document).ready(function() {
        if (url.indexOf('wcv-vendor-shopsettings') > 0 ) {
            
            $('<form id="form_kyc" action="" method="post" enctype="multipart/form-data" class="kyc_form kyc_form wcv-form wcv-formvalidator" style="margin-top:20px;"></form>').appendTo($('#wpbody-content'));
            $('#kyc_div_global').appendTo($('#form_kyc'));
            $('#kyc_div_global').show();
        }
    });
    
})( jQuery );