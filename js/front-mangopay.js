/* KYC ERROR MESSAGE */
(function($) {
   
	$(document).ready(function() {
        if($.magnificPopup != undefined && $.magnificPopup.instance != null){
            $.magnificPopup.instance._onFocusIn = function(e) {
                if( $(e.target).hasClass('ui-datepicker-year') ) {
                    return true;
                }
                $.magnificPopup.proto._onFocusIn.call(this,e);
            };
        }
	});
	

 
})( jQuery );
/* KYC tab show/hide form */
(function($) {
   
	$(document).ready(function() {
        
        if($('#kyc_div_global').attr('id') != undefined && $('.tabs-tab').attr('class') != undefined){
            $('.tabs-tab').click(function(){
                if($(this).attr('class') == 'tabs-tab  payment'){
                    $('#kyc_div_global').show();
                }else{
                    $('#kyc_div_global').hide();
                }
            })
        }
	});
	

 
})( jQuery );